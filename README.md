# Swin Transformer Models for Semantic Segmentation

Project adjusted from original repository https://github.com/SwinTransformer/Swin-Transformer-Semantic-Segmentation according to needs of the research. Implemented in Python using PyTorch library.

Researching, training and redesigning these Swin transformer models for semantic segmentation is part of my grad thesis deep learning project, which has the goal of comparing state-of-the-art transformer to convolutional models.

Training Swin models and measuring their memory/speed performance.

Project duration: Feb 2022 - July 2022.
