import argparse
import os

import mmcv
import torch
from mmcv.parallel import MMDataParallel, MMDistributedDataParallel
from mmcv.runner import get_dist_info, init_dist, load_checkpoint
from mmcv.utils import DictAction

from mmseg.apis import multi_gpu_test, single_gpu_test
from mmseg.datasets import build_dataloader, build_dataset
from mmseg.models import build_segmentor

from mmseg.models.backbones.swin_transformer import SwinTransformer

def measure_macs_params(model):
    model=model.cuda()
    from thop import profile
    inp = torch.randn(1, 3, 1024, 2048).to(next(model.parameters()).device)
    img_metas = [{
        'img_shape': (1024, 2048, 3),
        'ori_shape': (1024, 2048, 3),
        'pad_shape': (1024, 2048, 3),
        'filename': '<demo>.png',
        'scale_factor': 1.0,
        'flip': False,
        'flip_direction': 'horizontal'
    }]

    img_list = [img[None, :] for img in inp]
    img_meta_list = [[img_meta] for img_meta in img_metas]
    print("IMG LIST", len(img_list), [im.shape for im in img_list])
    print("DEV====", next(model.parameters()).device, inp.device)
    print("tr=", model.training)
    macs, params = profile(model, inputs=(img_list, img_meta_list, False))

    # from torchvision.models import resnet101
    # from thop import profile
    # model = resnet101().cuda(0)
    # input = torch.randn(1, 3, 224, 224).cuda(0)
    # macs, params = profile(model, inputs=(input, ))

    return  macs, params

import time
def do_eval_step(
                model,
                bs=None,
                im_sh=(768,768),#(1024,2048),
                imgs = None,
                img_metas = None):

    if imgs is None:
        torch.cuda.empty_cache()
        imgs = torch.randn(bs, 3, *im_sh).to(torch.device(0))

        img_metas = [{
            'img_shape': (*im_sh, 3),
            'ori_shape': (*im_sh, 3),
            'pad_shape': (*im_sh, 3),
            'filename': '<demo>.png',
            'scale_factor': 1.0,
            'flip': False,
            'flip_direction': 'horizontal'
        } for _ in range(bs)]

    #print("IM====",imgs.shape)
    #print("DEVS", imgs.get_device())
    #print("IS_CUDA", imgs.is_cuda)


    img_list = [img[None, :] for img in imgs]
    img_meta_list = [[img_meta] for img_meta in img_metas]

    torch.cuda.synchronize()
    start = time.time()

    seg_map = model.forward(img_list, img_meta_list, return_loss=False, ret_preds=True)

    torch.cuda.synchronize()
    end = time.time()

    torch.cuda.synchronize()

    return (end - start)


def get_max_batchsize_eval(
        model,
        distributed=False):

    if not distributed:
        model = MMDataParallel(model, device_ids=[0])
    else:
        model = MMDistributedDataParallel(
            model.cuda(),
            device_ids=[torch.cuda.current_device()],
            broadcast_buffers=False)

    # print("MODEL DEV", next(model.parameters()).device)

    model.eval()

    with torch.no_grad():
        cur_bs = 2
        while True:
            try:
                print("cur_bs =", cur_bs)
                do_eval_step(
                    model,
                    cur_bs)

                cur_bs *= 2
            except RuntimeError as err:
                print("!!! err =", err)
                top_bs = cur_bs
                bottom_bs = cur_bs // 2
                break

        print("bottom =", bottom_bs)
        print("top =", top_bs)
        while (top_bs - bottom_bs) > 1:
            mid_bs = (top_bs + bottom_bs) // 2
            print("mid_bs =", mid_bs)
            try:
                do_eval_step(
                    model,
                    mid_bs)
                bottom_bs = mid_bs
            except RuntimeError:
                top_bs = mid_bs

        print("bottom =", bottom_bs)
        print("top =", top_bs)
        try:
            do_eval_step(
                model,
                top_bs)
            max_bs = top_bs
        except RuntimeError as err:
            print("!!! err =", err)
            max_bs = bottom_bs

    return max_bs

def measure_speed_eval_fps(
        model,
        bs,
        im_sh=(768,768),#(1024,2048),
        distributed=False,
        n=30,
        inst_model=True):
    import torch
    if inst_model:
        if not distributed:
            model = MMDataParallel(model, device_ids=[0])
        else:
            model = MMDistributedDataParallel(
                model.cuda(),
                device_ids=[torch.cuda.current_device()],
                broadcast_buffers=False)

    #print("MODEL DEV", next(model.parameters()).device)

    model.eval()

    warm_up = 10

    ###############
    torch.cuda.empty_cache()
    imgs = torch.randn(bs, 3, *im_sh).to(torch.device(0))

    img_metas = [{
        'img_shape': (*im_sh, 3),
        'ori_shape': (*im_sh, 3),
        'pad_shape': (*im_sh, 3),
        'filename': '<demo>.png',
        'scale_factor': 1.0,
        'flip': False,
        'flip_direction': 'horizontal'
    } for _ in range(bs)]
    ###############

    #print("DEVS", imgs.get_device())
    #print("IS_CUDA", imgs.is_cuda)

    with torch.no_grad():
        for i in range(warm_up):
            print("i",i)
            do_eval_step(
                model,
                imgs=imgs,
                img_metas=img_metas)

    tot_time = 0
    with torch.no_grad():
        for _ in range(n):
            tot_time += do_eval_step(
                model,
                imgs=imgs,
                img_metas=img_metas)

    return (bs * n) / tot_time

def get_max_inputsize_eval(
    model,
    distributed=False,
    init_im_sh=(64,128),
):
    import torch

    if not distributed:
        model = MMDataParallel(model, device_ids=[0])
    else:
        model = MMDistributedDataParallel(
            model.cuda(),
            device_ids=[torch.cuda.current_device()],
            broadcast_buffers=False)

    model.eval()

    double_dim = lambda shape_tup : tuple(2*d for d in shape_tup)
    half_dim = lambda shape_tup: tuple(d//2 for d in shape_tup)

    with torch.no_grad():
        cur_im_sh = init_im_sh
        while True:
            try:
                print("cur_im_sh =", cur_im_sh)
                do_eval_step(
                    model,
                    im_sh=cur_im_sh,
                    bs=1,
                )

                cur_im_sh = double_dim(cur_im_sh)
            except RuntimeError as err:
                print("!!! err =", err)
                top_im_sh = cur_im_sh
                bottom_im_sh = half_dim(cur_im_sh)
                break

        print("bottom =", bottom_im_sh)
        print("top =", top_im_sh)
        while (top_im_sh[0] - bottom_im_sh[0]) > 1:
            mid_im_sh = half_dim(tuple(d1+d2 for d1,d2 in zip(top_im_sh, bottom_im_sh)))
            print("mid_im_sh =", mid_im_sh)
            try:
                do_eval_step(
                    model,
                    im_sh=mid_im_sh,
                    bs=1,
                )
                bottom_im_sh = mid_im_sh
            except RuntimeError:
                top_im_sh = mid_im_sh

        print("bottom =", bottom_im_sh)
        print("top =", top_im_sh)
        try:
            do_eval_step(
                model,
                im_sh=top_im_sh,
                bs=1,
            )
            max_im_sh = top_im_sh
        except RuntimeError as err:
            print("!!! err =", err)
            max_im_sh = bottom_im_sh

    return max_im_sh

def measure_speed_eval_wrt_inputsize(
        model,
        distributed=False,
        max_im_sh=(1024, 2048),
        n=100):
    import torch

    if not distributed:
        model = MMDataParallel(model, device_ids=[0])
    else:
        model = MMDistributedDataParallel(
            model.cuda(),
            device_ids=[torch.cuda.current_device()],
            broadcast_buffers=False)

    im_sh = (64, 128)
    max_im_h, max_im_w = max_im_sh
    #double_dim = lambda shape_tup: tuple(2 * d for d in shape_tup)
    add_dim = lambda shape_tup: (shape_tup[0] + 16, shape_tup[1] + 32)

    print("im_sh FPS")
    while im_sh[0] < max_im_h:
        speed_result = measure_speed_eval_fps(
            model,
            bs=1,
            im_sh=im_sh,
            n=n,
            inst_model=False)
        print(im_sh, speed_result)

        im_sh = add_dim(im_sh) #double_dim(im_sh)


    speed_result = measure_speed_eval_fps(
        model,
        bs=1,
        im_sh=max_im_sh,
        n=n,
        inst_model=False)
    print(max_im_sh, speed_result)

def parse_args():
    parser = argparse.ArgumentParser(
        description='mmseg test (and eval) a model')
    # LEO - BEGIN
    parser.add_argument('--measure-macs')
    parser.add_argument('--get-max-bs-eval')
    parser.add_argument('--measure-speed-eval')
    parser.add_argument('--measure-speed-eval-batchsize')
    parser.add_argument("--get-max-inputsize-eval")
    parser.add_argument("--measure-speed-eval-wrt-inputsize", type=int, default=0)  # dim h - from im_sh = (h, w); w = 2*h
    parser.add_argument("--measure-speed-eval-wrt-inputsize-maxinputsize", type=int, default=None)
    parser.add_argument("--measure-speed-eval-wrt-inputsize-iters", type=int, default=100)
    # parser.add_argument('--get-segmaps', default=None)
    # parser.add_argument('--input-dir', type=str, default=None)
    # parser.add_argument('--output-dir', type=str, default=None)
    parser.add_argument('--show-attn-map', default=None)
    parser.add_argument('--output-dir', type=str, default=None)
    parser.add_argument('--stage-id', default=0, type=int)
    parser.add_argument('--layer-id', default=0, type=int)
    parser.add_argument('--x-patch', default=0, type=int)
    parser.add_argument('--y-patch', default=0, type=int)
    parser.add_argument("--cmap", default="viridis", type=str)
    # parser.add_argument("--enc/--dec", default=None)
    parser.add_argument("--cls", default=None)
    # LEO - END
    parser.add_argument('config', help='test config file path')
    parser.add_argument('checkpoint', help='checkpoint file')
    parser.add_argument(
        '--aug-test', action='store_true', help='Use Flip and Multi scale aug')
    parser.add_argument('--out', help='output result file in pickle format')
    parser.add_argument(
        '--format-only',
        action='store_true',
        help='Format the output results without perform evaluation. It is'
        'useful when you want to format the result to a specific format and '
        'submit it to the test server')
    parser.add_argument(
        '--eval',
        type=str,
        nargs='+',
        help='evaluation metrics, which depends on the dataset, e.g., "mIoU"'
        ' for generic datasets, and "cityscapes" for Cityscapes')
    parser.add_argument('--show', action='store_true', help='show results')
    parser.add_argument(
        '--show-dir', help='directory where painted images will be saved')
    parser.add_argument(
        '--gpu-collect',
        action='store_true',
        help='whether to use gpu to collect results.')
    parser.add_argument(
        '--tmpdir',
        help='tmp directory used for collecting results from multiple '
        'workers, available when gpu_collect is not specified')
    parser.add_argument(
        '--options', nargs='+', action=DictAction, help='custom options')
    parser.add_argument(
        '--eval-options',
        nargs='+',
        action=DictAction,
        help='custom options for evaluation')
    parser.add_argument(
        '--launcher',
        choices=['none', 'pytorch', 'slurm', 'mpi'],
        default='none',
        help='job launcher')
    parser.add_argument('--local_rank', type=int, default=0)
    args = parser.parse_args()
    if 'LOCAL_RANK' not in os.environ:
        os.environ['LOCAL_RANK'] = str(args.local_rank)
    return args


# def seg_to_rgb(seg, colors):
#     seg = torch.from_numpy(seg).unsqueeze(0)
#     #print("SEG:SHAPE", seg.shape)
#     im = torch.zeros((seg.shape[0], seg.shape[1], seg.shape[2], 3)).float()
#     cls = torch.unique(seg)
#     for cl in cls:
#         color = colors[int(cl)]
#         if len(color.shape) > 1:
#             color = color[0]
#         im[seg == cl] = color
#     return im
#
# import yaml
# IGNORE_LABEL = 255
# def dataset_cat_description(path, cmap=None):
#     desc = yaml.load(open(path, "r"), Loader=yaml.FullLoader)
#     colors = {}
#     names = []
#     for i, cat in enumerate(desc):
#         names.append(cat["name"])
#         if "color" in cat:
#             colors[cat["id"]] = torch.tensor(cat["color"]).float() / 255
#         else:
#             colors[cat["id"]] = torch.tensor(cmap[cat["id"]]).float()
#     colors[IGNORE_LABEL] = torch.tensor([0.0, 0.0, 0.0]).float()
#     return names, colors

# available only ENCODER attn maps (SwinTransformer); decoder is not a transformer model
def visualize(
    model,
    data_loader,
    image_size,
    output_dir,
    stage_id,
    layer_id,
    x_patch,
    y_patch,
    cmap,
    cls,
):
    import einops
    from pathlib import Path
    from PIL import Image
    from torchvision import transforms
    import torch.nn.functional as F
    import matplotlib.pyplot as plt
    import torchvision
    from math import sqrt

    output_dir = Path(output_dir)

    for p in model.parameters():
        p.requires_grad = False

    model.eval()
    model.to(torch.device(1))

    # Get model config
    patch_size = 4 # hardcoded for 3 Swin configs in configs/swin

    # Open image and process it
    for data in data_loader:
        img = data["img"][0].squeeze(0)
        break
    print("IMG====",img.shape)
    print("img size", image_size)
    # Make the image divisible by the patch size
    w, h = (
        image_size - image_size % patch_size,
        image_size - image_size % patch_size,
    )

    # Crop to image size
    img = img[:, :w, :h].unsqueeze(0)
    print("IMG aft crop====",img.shape)
    print(".....img shape w h", img.shape, "patch sze", patch_size)

    #w_featmap = img.shape[-2] // patch_size
    #h_featmap = img.shape[-1] // patch_size

    #print("W H featmap", w_featmap, h_featmap)

    # Sanity checks
    if not isinstance(model.backbone, SwinTransformer):
        raise ValueError(
            f"Attention maps for decoder are only available for SwinTransformer. Provided model with encoder type: {model.backbone}."
        )

    #if not cls:
    #    if x_patch > w_featmap or y_patch > h_featmap:
    #        raise ValueError(
    #            f"Provided patch x: {x_patch} y: {y_patch} is not valid. Patch should be in the range x: [0, {w_featmap}), y: [0, {h_featmap})"
    #        )
    #    num_patch = w_featmap * y_patch + x_patch
    #    print("numpatch", w_featmap, h_featmap, num_patch)

    if stage_id < 0:
        raise ValueError("Provided stage_id should be positive.")
    if model.backbone.num_layers <= stage_id:
        raise ValueError(
            f"Provided stage_id: {stage_id} is not valid for encoder with {model.backbone.num_layers} stages."
        )

    if layer_id < 0:
        raise ValueError("Provided layer_id should be positive.")
    if model.backbone.layers[stage_id].depth <= layer_id:
        raise ValueError(
            f"Provided layer_id: {layer_id} is not valid for encoder stage {stage_id} with {model.backbone.layers[stage_id].depth} layers."
        )

    Path.mkdir(output_dir, exist_ok=True)

    # Process input and extract attention maps

    print(f"Generating Attention Mapping for Encoder Stage Id {stage_id} - Layer Id {layer_id}")
    attentions = model.get_attention_map_enc(img.to(torch.device(1)), stage_id, layer_id)
    print("ATTN====",attentions.shape)
    # LEO - BEG
    w_featmap = h_featmap = int(sqrt(attentions.shape[2]))
    print("W H featmap", w_featmap, h_featmap)
    if not cls:
        if x_patch > w_featmap or y_patch > h_featmap:
            raise ValueError(
                f"Provided patch x: {x_patch} y: {y_patch} is not valid. Patch should be in the range x: [0, {w_featmap}), y: [0, {h_featmap})"
            )
        num_patch = w_featmap * y_patch + x_patch
        print("numpatch", w_featmap, h_featmap, num_patch)

    # LEO - END
    if cls:
        attentions = attentions[0, :, 0, :]
    else:
        attentions = attentions[
            0, :, num_patch, :
        ]

    print("ATN1",attentions.shape)
    # Reshape into image shape
    nh = attentions.shape[0]  # Number of heads
    attentions = attentions.reshape(nh, -1)
    print("ATN2",attentions.shape)


    attentions = attentions.reshape(nh, 1, w_featmap, h_featmap)
    print("****SCALE FACTOR", img.shape[-1]//w_featmap, "now", patch_size)
    # Resize attention maps to match input size
    attentions = (
        F.interpolate(attentions, scale_factor=img.shape[-1]//w_featmap, mode="nearest").cpu().numpy()
    )
    #attentions = (
    #    F.interpolate(attentions, scale_factor=patch_size, mode="nearest").cpu().numpy()
    #)


    # Save Attention map for each head
    for i in range(nh):
        base_name = "enc"
        head_name = f"{base_name}_stage{stage_id}_layer{layer_id}_attn-head{i}"
        attention_maps_list = attentions[i]
        for j in range(attention_maps_list.shape[0]):
            attention_map = attention_maps_list[j]
            file_name = head_name
            dir_path = output_dir / f"{base_name}_stage{stage_id}_layer{layer_id}"
            Path.mkdir(dir_path, exist_ok=True)
            if cls:
                file_name = f"{file_name}_cls"
                dir_path /= "cls"
                Path.mkdir(dir_path, exist_ok=True)
            else:
                dir_path /= f"patch_{x_patch}_{y_patch}"
                Path.mkdir(dir_path, exist_ok=True)

            file_path = dir_path / f"{file_name}.png"
            plt.imsave(fname=str(file_path), arr=attention_map, format="png", cmap=cmap)
            print(f"{file_path} saved.")

    # Save input image showing selected patch
    if not cls:
        im_n = torchvision.utils.make_grid(img, normalize=True, scale_each=True)

        # Compute corresponding X and Y px in the original image
        x_px = x_patch * patch_size
        y_px = y_patch * patch_size
        px_v = einops.repeat(
            torch.tensor([1, 0, 0]),
            "c -> 1 c h w",
            h=patch_size,
            w=patch_size,
        )

        # Draw pixels for selected patch
        im_n[:, y_px : y_px + patch_size, x_px : x_px + patch_size] = px_v
        torchvision.utils.save_image(
            im_n,
            str(dir_path / "input_img.png"),
        )

def main():
    args = parse_args()

    #assert args.out or args.eval or args.format_only or args.show \
    #    or args.show_dir, \
    #    ('Please specify at least one operation (save/eval/format/show the '
    #     'results / save the results) with the argument "--out", "--eval"'
    #     ', "--format-only", "--show" or "--show-dir"')

    if args.eval and args.format_only:
        raise ValueError('--eval and --format_only cannot be both specified')

    if args.out is not None and not args.out.endswith(('.pkl', '.pickle')):
        raise ValueError('The output file must be a pkl file.')

    cfg = mmcv.Config.fromfile(args.config)
    if args.options is not None:
        cfg.merge_from_dict(args.options)
    # set cudnn_benchmark
    if cfg.get('cudnn_benchmark', False):
        torch.backends.cudnn.benchmark = True
    if args.aug_test:
        # hard code index
        cfg.data.test.pipeline[1].img_ratios = [
            0.5, 0.75, 1.0, 1.25, 1.5, 1.75
        ]
        cfg.data.test.pipeline[1].flip = True
    cfg.model.pretrained = None
    cfg.data.test.test_mode = True

    # init distributed env first, since logger depends on the dist info.
    print("LAUNCHER", args.launcher)
    if args.launcher == 'none':
        distributed = False
    else:
        distributed = True
        init_dist(args.launcher, **cfg.dist_params)

    if args.get_max_bs_eval:
        cfg.model.train_cfg = None
        model = build_segmentor(cfg.model, test_cfg=cfg.get('test_cfg'))
        checkpoint = load_checkpoint(model, args.checkpoint, map_location='cpu')
        model.CLASSES = checkpoint['meta']['CLASSES']
        model.PALETTE = checkpoint['meta']['PALETTE']

        max_bs_result = get_max_batchsize_eval(
            model,
            distributed=distributed
        )
        print("MAX BS =", max_bs_result)
        return
    elif args.measure_speed_eval:
        cfg.model.train_cfg = None
        model = build_segmentor(cfg.model, test_cfg=cfg.get('test_cfg'))
        checkpoint = load_checkpoint(model, args.checkpoint, map_location='cpu')
        model.CLASSES = checkpoint['meta']['CLASSES']
        model.PALETTE = checkpoint['meta']['PALETTE']

        speed_result = measure_speed_eval_fps(
            model,
            int(args.measure_speed_eval_batchsize),
            distributed=distributed
        )
        print("FPS = ", speed_result)
        return
    elif args.get_max_inputsize_eval:
        cfg.model.train_cfg = None
        model = build_segmentor(cfg.model, test_cfg=cfg.get('test_cfg'))
        checkpoint = load_checkpoint(model, args.checkpoint, map_location='cpu')
        model.CLASSES = checkpoint['meta']['CLASSES']
        model.PALETTE = checkpoint['meta']['PALETTE']

        max_inputsize_result = get_max_inputsize_eval(
            model,
            distributed=distributed
        )
        print("MAX INPUTSIZE =", max_inputsize_result)
        return
    elif args.measure_speed_eval_wrt_inputsize:
        cfg.model.train_cfg = None
        model = build_segmentor(cfg.model, test_cfg=cfg.get('test_cfg'))
        checkpoint = load_checkpoint(model, args.checkpoint, map_location='cpu')
        model.CLASSES = checkpoint['meta']['CLASSES']
        model.PALETTE = checkpoint['meta']['PALETTE']

        max_im_h, max_im_w = args.measure_speed_eval_wrt_inputsize_maxinputsize, 2*args.measure_speed_eval_wrt_inputsize_maxinputsize
        measure_speed_eval_wrt_inputsize(model, distributed=distributed, max_im_sh=(max_im_h, max_im_w),
                                         n=args.measure_speed_eval_wrt_inputsize_iters)
    elif args.measure_macs:
        # build the model and load checkpoint
        cfg.model.train_cfg = None
        model = build_segmentor(cfg.model, test_cfg=cfg.get('test_cfg'))
        checkpoint = load_checkpoint(model, args.checkpoint, map_location='cpu')
        model.CLASSES = checkpoint['meta']['CLASSES']
        model.PALETTE = checkpoint['meta']['PALETTE']

        macs, params = measure_macs_params(model)

        print("MACS", macs)
        print("PARAMS", params)
        return
        # END
    elif args.show_attn_map:
        cfg.model.train_cfg = None
        model = build_segmentor(cfg.model, test_cfg=cfg.get('test_cfg'))
        checkpoint = load_checkpoint(model, args.checkpoint, map_location='cpu')
        model.CLASSES = checkpoint['meta']['CLASSES']
        model.PALETTE = checkpoint['meta']['PALETTE']

        dataset = build_dataset(cfg.data.test)
        data_loader = build_dataloader(
            dataset,
            samples_per_gpu=1,
            workers_per_gpu=cfg.data.workers_per_gpu,
            dist=distributed,
            shuffle=False)

        visualize(
            model,
            data_loader,
            cfg.crop_size[0],
            args.output_dir,
            args.stage_id,
            args.layer_id,
            args.x_patch,
            args.y_patch,
            args.cmap,
            args.cls,
        )
        return
    # elif args.get_segmaps:
    #     cfg.model.train_cfg = None
    #     model = build_segmentor(cfg.model, test_cfg=cfg.get('test_cfg'))
    #     checkpoint = load_checkpoint(model, args.checkpoint, map_location='cpu')
    #     model.CLASSES = checkpoint['meta']['CLASSES']
    #     model.PALETTE = checkpoint['meta']['PALETTE']
    #
    #     if not distributed:
    #         model = MMDataParallel(model, device_ids=[1])
    #     else:
    #         model = MMDistributedDataParallel(
    #             model.cuda(),
    #             device_ids=[torch.cuda.current_device()],
    #             broadcast_buffers=False)
    #
    #     model.eval()
    #
    #     from pathlib import Path
    #     from PIL import Image
    #     from tqdm import tqdm
    #     #import torchvision.transforms.functional as F
    #     from mmcv.image import tensor2imgs
    #     import numpy as np
    #
    #     normalization = cfg.img_norm_cfg
    #
    #     ADE20K_CATS_PATH = Path(__file__).parent / "ade20k.yml"
    #
    #     cat_names, cat_colors = dataset_cat_description(ADE20K_CATS_PATH)
    #
    #     # input_dir = Path(args.input_dir)
    #     output_dir = Path(args.output_dir)
    #     output_dir.mkdir(exist_ok=True)
    #
    #     # list_dir = list(input_dir.iterdir())
    #
    #     dataset = build_dataset(cfg.data.test)
    #     data_loader = build_dataloader(
    #         dataset,
    #         samples_per_gpu=1,
    #         workers_per_gpu=cfg.data.workers_per_gpu,
    #         dist=distributed,
    #         shuffle=False)
    #
    #     # for filename in tqdm(list_dir, ncols=80):
    #     #     pil_im = Image.open(filename).copy()
    #     #     im = F.pil_to_tensor(pil_im).float() / 255
    #     #     im = F.normalize(im, normalization.mean, normalization.std)
    #     #     im = im.to(torch.device(1)).unsqueeze(0)
    #     #
    #     #     im_meta = [dict(ori_shape=(im.shape[2], im.shape[3], im.shape[1]), img_shape=(im.shape[2], im.shape[3], im.shape[1]), pad_shape=(im.shape[2], im.shape[3], im.shape[1]), flip=False)]
    #     #
    #     #     #print(">>>>>>>>>>>>>im", im.shape)
    #     #     with torch.no_grad():
    #     #         img_list = [img[None, :] for img in im]
    #     #         img_meta_list = [[img_meta] for img_meta in im_meta]
    #     #         #print(im.shape, "im_list", len(img_list), img_list[0].shape)
    #     #         logits = model.forward(img_list, img_meta_list, return_loss=False)
    #     #     #print("LOGS", type(logits), len(logits), logits[0].shape)
    #
    #     for data in tqdm(data_loader):
    #         img_tensor = data['img'][0]
    #         img_metas = data['img_metas'][0].data[0]
    #
    #         imgs = tensor2imgs(img_tensor, **img_metas[0]['img_norm_cfg'])
    #         assert len(imgs) == len(img_metas)
    #
    #         with torch.no_grad():
    #             logits = model.forward(return_loss=False, **data)
    #         #print("LOGS", type(logits), len(logits), logits[0].shape)
    #
    #         seg_map = logits[0]
    #         seg_rgb = seg_to_rgb(seg_map, cat_colors)
    #         seg_rgb = (255 * seg_rgb.cpu().numpy()).astype(np.uint8)
    #         pil_seg = Image.fromarray(seg_rgb[0])
    #
    #         pil_blend = Image.blend(pil_im, pil_seg, 0.5).convert("RGB")
    #         pil_blend.save(output_dir / filename.name)
    #
    #     return


    # build the dataloader
    # TODO: support multiple images per gpu (only minor changes are needed)
    dataset = build_dataset(cfg.data.test)
    data_loader = build_dataloader(
        dataset,
        samples_per_gpu=1,
        workers_per_gpu=cfg.data.workers_per_gpu,
        dist=distributed,
        shuffle=False)

    # build the model and load checkpoint
    cfg.model.train_cfg = None
    model = build_segmentor(cfg.model, test_cfg=cfg.get('test_cfg'))
    checkpoint = load_checkpoint(model, args.checkpoint, map_location='cpu')
    model.CLASSES = checkpoint['meta']['CLASSES']
    model.PALETTE = checkpoint['meta']['PALETTE']

    efficient_test = False
    if args.eval_options is not None:
        efficient_test = args.eval_options.get('efficient_test', False)

    if not distributed:
        model = MMDataParallel(model, device_ids=[0])
        outputs = single_gpu_test(model, data_loader, args.show, args.show_dir,
                                  efficient_test)
    else:
        model = MMDistributedDataParallel(
            model.cuda(),
            device_ids=[torch.cuda.current_device()],
            broadcast_buffers=False)
        outputs = multi_gpu_test(model, data_loader, args.tmpdir,
                                 args.gpu_collect, efficient_test)

    rank, _ = get_dist_info()
    if rank == 0:
        if args.out:
            print(f'\nwriting results to {args.out}')
            mmcv.dump(outputs, args.out)
        kwargs = {} if args.eval_options is None else args.eval_options
        if args.format_only:
            dataset.format_results(outputs, **kwargs)
        if args.eval:
            dataset.evaluate(outputs, args.eval, **kwargs)


if __name__ == '__main__':
    main()
