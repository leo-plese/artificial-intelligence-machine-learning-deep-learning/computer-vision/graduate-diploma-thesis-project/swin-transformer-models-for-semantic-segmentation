import argparse
import copy
import os
import os.path as osp
import time

import mmcv
import torch
from mmcv.runner import init_dist
from mmcv.utils import Config, DictAction, get_git_hash

from mmseg import __version__
from mmseg.apis import set_random_seed, train_segmentor
from mmseg.datasets import build_dataset
from mmseg.models import build_segmentor
from mmseg.utils import collect_env, get_root_logger
from mmcv.parallel import MMDataParallel, MMDistributedDataParallel
from mmcv.runner import build_optimizer
from mmseg.datasets import build_dataloader


def do_train_step_batch(
                model,
                bs,
                optimizer,
                crop_size=480,
                num_classes=60,
                data_loader = None,
                ds = None,
                cfg = None,
                distributed = None,
                n = 1):
    if data_loader is None:
        torch.cuda.empty_cache()
        #ds = build_dataset(cfg.data.train)
        data_loader = build_dataloader(
            ds,
            bs,
            cfg.data.workers_per_gpu,
            # cfg.gpus will be ignored if distributed
            len(cfg.gpu_ids),
            dist=distributed,
            seed=cfg.seed,
            drop_last=True)


    i = 0
    for batch in data_loader:
        print("i", i)
        if i == n:
            return

        torch.cuda.empty_cache()
        optimizer.zero_grad()
        losses = model.forward(batch["img"], batch["img_metas"], gt_semantic_seg=batch["gt_semantic_seg"], return_loss=True)
        optimizer.step()

        i += 1
        del losses
        torch.cuda.synchronize()



def do_train_step(
                model,
                bs,
                optimizer,
                crop_size=480,
                num_classes=60,
                imgs = None,
                img_metas = None,
                gt_semantic_seg = None,
                record_memory=False):
    if imgs is None:
        torch.cuda.empty_cache()
        imgs = torch.randn(bs, 3, crop_size, crop_size).to(torch.device(1))
        gt_semantic_seg = torch.randint(low=0, high=num_classes - 1, size=(bs, 1, crop_size, crop_size)).to(torch.device(1))

        img_metas = [{
            'img_shape': (crop_size, crop_size, 3),
            'ori_shape': (crop_size, crop_size, 3),
            'pad_shape': (crop_size, crop_size, 3),
            'filename': '<demo>.png',
            'scale_factor': 1.0,
            'flip': False,
            'flip_direction': 'horizontal'
        } for _ in range(bs)]


    #print("IM====",imgs.shape)
    #print("DEVS", imgs.get_device(), gt_semantic_seg.get_device())
    #print("IS_CUDA", imgs.is_cuda, gt_semantic_seg.is_cuda)

    optimizer.zero_grad()
    losses = None
    try:
        losses = model.forward(imgs, img_metas, gt_semantic_seg=gt_semantic_seg, return_loss=True)
    except ValueError as exc:
        print("ignore exc", type(exc), exc)
    optimizer.step()

    del losses

    torch.cuda.synchronize()

    if record_memory:
        return torch.cuda.max_memory_allocated(torch.device(1))


def get_max_batchsize_train(
        model,
        cfg,
        distributed=False):

    # put model on gpus
    if distributed:
        find_unused_parameters = cfg.get('find_unused_parameters', False)
        # Sets the `find_unused_parameters` parameter in
        # torch.nn.parallel.DistributedDataParallel
        model = MMDistributedDataParallel(
            model.cuda(),
            device_ids=[torch.cuda.current_device()],
            broadcast_buffers=False,
            find_unused_parameters=find_unused_parameters)
    else:
        model = MMDataParallel(model, device_ids=[1])

    #print("MODEL DEV", next(model.parameters()).device)
    model.train()

    optimizer = build_optimizer(model, cfg.optimizer)
    ds = build_dataset(cfg.data.train)

    cur_bs = 2
    while True:
        try:
            print("cur_bs =", cur_bs)
            #do_train_step(
            #    model,
            #    cur_bs,
            #    optimizer)
            
            do_train_step_batch(
                model,
                cur_bs,
                optimizer,
                ds = ds,
                cfg = cfg,
                distributed = distributed)

            cur_bs *= 2
        except RuntimeError as err:
            print("!!! err =", err)
            top_bs = cur_bs
            bottom_bs = cur_bs // 2
            break

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    while (top_bs - bottom_bs) > 1:
        mid_bs = (top_bs + bottom_bs) // 2
        print("mid_bs =", mid_bs)
        try:
            #do_train_step(
            #    model,
            #    mid_bs,
            #    optimizer)
            do_train_step_batch(
                model,
                mid_bs,
                optimizer,
                ds = ds,
                cfg = cfg,
                distributed = distributed)
            bottom_bs = mid_bs
        except RuntimeError:
            top_bs = mid_bs

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    try:
        cfg.data.samples_per_gpu = top_bs
        #do_train_step(
        #    model,
        #    top_bs,
        #    optimizer)
        do_train_step_batch(
                model,
                top_bs,
                optimizer,
                ds = ds,
                cfg = cfg,
                distributed = distributed)
        max_bs = top_bs
    except RuntimeError as err:
        print("!!! err =", err)
        max_bs = bottom_bs

    return max_bs

def measure_speed_train_fps(
        model,
        bs,
        cfg,
        distributed=False,
        crop_size=480,
        num_classes=60,
        n=30):

    import torch
    import time

    # put model on gpus
    if distributed:
        find_unused_parameters = cfg.get('find_unused_parameters', False)
        # Sets the `find_unused_parameters` parameter in
        # torch.nn.parallel.DistributedDataParallel
        model = MMDistributedDataParallel(
            model.cuda(),
            device_ids=[torch.cuda.current_device()],
            broadcast_buffers=False,
            find_unused_parameters=find_unused_parameters)
    else:
        model = MMDataParallel(model, device_ids=[1])

    #print("MODEL DEV", next(model.parameters()).device)

    model.train()

    optimizer = build_optimizer(model, cfg.optimizer)

    warm_up = 10

    ###############
    torch.cuda.empty_cache()
    ds = build_dataset(cfg.data.train)
    data_loader = build_dataloader(
            ds,
            bs,
            cfg.data.workers_per_gpu,
            # cfg.gpus will be ignored if distributed
            len(cfg.gpu_ids),
            dist=distributed,
            seed=cfg.seed,
            drop_last=True)

    #imgs = torch.randn(bs, 3, crop_size, crop_size).to(torch.device(1))
    #gt_semantic_seg = torch.randint(low=0, high=num_classes - 1, size=(bs, 1, crop_size, crop_size)).to(torch.device(1))

    #img_metas = [{
    #    'img_shape': (crop_size, crop_size, 3),
    #    'ori_shape': (crop_size, crop_size, 3),
    #    'pad_shape': (crop_size, crop_size, 3),
    #    'filename': '<demo>.png',
    #    'scale_factor': 1.0,
    #    'flip': False,
    #    'flip_direction': 'horizontal'
    #} for _ in range(bs)]
    ###############

    #print("DEVS", imgs.get_device(), gt_semantic_seg.get_device())
    #print("IS_CUDA", imgs.is_cuda, gt_semantic_seg.is_cuda)


    torch.cuda.synchronize()
    do_train_step_batch(
                model,
                bs,
                optimizer,
                data_loader = data_loader,
                n = warm_up)

    #for i in range(warm_up):
    #    torch.cuda.empty_cache()
    #    #print("i",i)
    #    do_train_step(
    #        model,
    #        bs,
    #        optimizer,
    #        imgs=imgs,
    #        img_metas=img_metas,
    #        gt_semantic_seg=gt_semantic_seg)

    start = time.time()

    do_train_step_batch(
                model,
                bs,
                optimizer,
                data_loader = data_loader,
                n = n)
    #for i in range(n):
    #    torch.cuda.empty_cache()
    #    #print("ii",i)
    #    do_train_step(
    #        model,
    #        bs,
    #        optimizer,
    #        imgs=imgs,
    #        img_metas=img_metas,
    #        gt_semantic_seg=gt_semantic_seg)

    torch.cuda.synchronize()

    end = time.time()
    return (bs * n) / (end - start)



def parse_args():
    parser = argparse.ArgumentParser(description='Train a segmentor')
    # LEO - BEGIN
    parser.add_argument('--log-memory')
    parser.add_argument('--get-max-bs-train')
    parser.add_argument('--measure-speed-train')
    parser.add_argument('--measure-speed-train-batchsize')
    # LEO - END
    parser.add_argument('config', help='train config file path')
    parser.add_argument('--work-dir', help='the dir to save logs and models')
    parser.add_argument(
        '--load-from', help='the checkpoint file to load weights from')
    parser.add_argument(
        '--resume-from', help='the checkpoint file to resume from')
    parser.add_argument(
        '--no-validate',
        action='store_true',
        help='whether not to evaluate the checkpoint during training')
    group_gpus = parser.add_mutually_exclusive_group()
    group_gpus.add_argument(
        '--gpus',
        type=int,
        help='number of gpus to use '
        '(only applicable to non-distributed training)')
    group_gpus.add_argument(
        '--gpu-ids',
        type=int,
        nargs='+',
        help='ids of gpus to use '
        '(only applicable to non-distributed training)')
    parser.add_argument('--seed', type=int, default=None, help='random seed')
    parser.add_argument(
        '--deterministic',
        action='store_true',
        help='whether to set deterministic options for CUDNN backend.')
    parser.add_argument(
        '--options', nargs='+', action=DictAction, help='custom options')
    parser.add_argument(
        '--launcher',
        choices=['none', 'pytorch', 'slurm', 'mpi'],
        default='none',
        help='job launcher')
    parser.add_argument('--local_rank', type=int, default=0)
    args = parser.parse_args()
    if 'LOCAL_RANK' not in os.environ:
        os.environ['LOCAL_RANK'] = str(args.local_rank)

    # LEO - BEGIN
    if "SLURM_STEPS_GPUS" in os.environ:
        gpu_ids = os.environ["SLURM_STEP_GPUS"].split(",")
        os.environ["MASTER_PORT"] = str(12345 + int(min(gpu_ids)))
    else:
        os.environ["MASTER_PORT"] = str(12345)


    if "SLURM_JOB_NODELIST" in os.environ:
        hostnames = hostlist.expand_hostlist(os.environ["SLURM_JOB_NODELIST"])
        os.environ["MASTER_ADDR"] = hostnames[0]
    else:
        os.environ["MASTER_ADDR"] = "127.0.0.1"
    # LEO - END

    return args


def main():
    args = parse_args()

    cfg = Config.fromfile(args.config)
    if args.options is not None:
        cfg.merge_from_dict(args.options)
    # set cudnn_benchmark
    if cfg.get('cudnn_benchmark', False):
        torch.backends.cudnn.benchmark = True

    # work_dir is determined in this priority: CLI > segment in file > filename
    if args.work_dir is not None:
        # update configs according to CLI args if args.work_dir is not None
        cfg.work_dir = args.work_dir
    elif cfg.get('work_dir', None) is None:
        # use config filename as default work_dir if cfg.work_dir is None
        cfg.work_dir = osp.join('./work_dirs',
                                osp.splitext(osp.basename(args.config))[0])
    if args.load_from is not None:
        cfg.load_from = args.load_from
    if args.resume_from is not None:
        cfg.resume_from = args.resume_from
    if args.gpu_ids is not None:
        cfg.gpu_ids = args.gpu_ids
    else:
        cfg.gpu_ids = range(1) if args.gpus is None else range(args.gpus)

    # init distributed env first, since logger depends on the dist info.
    if args.launcher == 'none':
        distributed = False
        # LEO - BEGIN
        import torch.distributed as dist
        dist.init_process_group("nccl", rank=0, world_size=1)
        # LEO - END
    else:
        distributed = True
        init_dist(args.launcher, **cfg.dist_params)

    print("DISTRIBUTED =", distributed)
    # create work_dir
    mmcv.mkdir_or_exist(osp.abspath(cfg.work_dir))
    # dump config
    cfg.dump(osp.join(cfg.work_dir, osp.basename(args.config)))
    # init the logger before other steps
    timestamp = time.strftime('%Y%m%d_%H%M%S', time.localtime())
    log_file = osp.join(cfg.work_dir, f'{timestamp}.log')
    logger = get_root_logger(log_file=log_file, log_level=cfg.log_level)

    # init the meta dict to record some important information such as
    # environment info and seed, which will be logged
    meta = dict()
    # log env info
    env_info_dict = collect_env()
    env_info = '\n'.join([f'{k}: {v}' for k, v in env_info_dict.items()])
    dash_line = '-' * 60 + '\n'
    logger.info('Environment info:\n' + dash_line + env_info + '\n' +
                dash_line)
    meta['env_info'] = env_info

    # log some basic info
    logger.info(f'Distributed training: {distributed}')
    logger.info(f'Config:\n{cfg.pretty_text}')

    # set random seeds
    if args.seed is not None:
        logger.info(f'Set random seed to {args.seed}, deterministic: '
                    f'{args.deterministic}')
        set_random_seed(args.seed, deterministic=args.deterministic)
    cfg.seed = args.seed
    meta['seed'] = args.seed
    meta['exp_name'] = osp.basename(args.config)

    model = build_segmentor(
        cfg.model,
        train_cfg=cfg.get('train_cfg'),
        test_cfg=cfg.get('test_cfg'))

    logger.info(model)

    if args.get_max_bs_train:
        max_bs_result = get_max_batchsize_train(
            model,
            cfg,
            distributed=distributed
        )
        print("MAX BS =", max_bs_result)
        return
    elif args.measure_speed_train:
        speed_result = measure_speed_train_fps(
            model,
            int(args.measure_speed_train_batchsize),
            cfg,
            distributed=distributed
        )
        print("FPS = ", speed_result)
        return
    elif args.log_memory:
        # put model on gpus
        if distributed:
            find_unused_parameters = cfg.get('find_unused_parameters', False)
            # Sets the `find_unused_parameters` parameter in
            # torch.nn.parallel.DistributedDataParallel
            model = MMDistributedDataParallel(
                model.cuda(),
                device_ids=[torch.cuda.current_device()],
                broadcast_buffers=False,
                find_unused_parameters=find_unused_parameters)
        else:
            model = MMDataParallel(model, device_ids=[1])

        # print("MODEL DEV", next(model.parameters()).device)
        model.train()

        optimizer = build_optimizer(model, cfg.optimizer)

        mem_alloc_before_train = torch.cuda.memory_allocated(torch.device(1))
        print("MEMORY allocated before TRAIN:", mem_alloc_before_train)

        do_train_step(
            model,
            bs=1,
            optimizer=optimizer)

        max_mem_alloc_during_train = do_train_step(
            model,
            bs=1,
            optimizer=optimizer,
            record_memory=True)

        print("MEMORY:")
        print("mem alloc before train:", mem_alloc_before_train)
        print("max memory alloc during train =", max_mem_alloc_during_train)
        GB = 1024.0 * 1024.0 * 1024.0
        mem_reqs_train = max_mem_alloc_during_train - mem_alloc_before_train
        print("MEMORY REQUIREMENTS FOR TRAIN = {}   ({} GB)".format(mem_reqs_train, mem_reqs_train / GB))

        return


    datasets = [build_dataset(cfg.data.train)]
    if len(cfg.workflow) == 2:
        val_dataset = copy.deepcopy(cfg.data.val)
        val_dataset.pipeline = cfg.data.train.pipeline
        datasets.append(build_dataset(val_dataset))
    if cfg.checkpoint_config is not None:
        # save mmseg version, config file content and class names in
        # checkpoints as meta data
        cfg.checkpoint_config.meta = dict(
            mmseg_version=f'{__version__}+{get_git_hash()[:7]}',
            config=cfg.pretty_text,
            CLASSES=datasets[0].CLASSES,
            PALETTE=datasets[0].PALETTE)
    # add an attribute for visualization convenience
    model.CLASSES = datasets[0].CLASSES
    train_segmentor(
        model,
        datasets,
        cfg,
        distributed=distributed,
        validate=(not args.no_validate),
        timestamp=timestamp,
        meta=meta)


if __name__ == '__main__':
    main()
